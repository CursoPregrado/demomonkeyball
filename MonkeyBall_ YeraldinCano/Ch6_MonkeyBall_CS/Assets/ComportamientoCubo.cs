﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ComportamientoCubo : MonoBehaviour {
    public Rigidbody rig;
    private Vector3 posInicial;
    public int fuerza = 3;

	// Use this for initialization
	void Start () {
        rig = this.GetComponent<Rigidbody>();
        posInicial = this.GetComponent<Transform>().position;

	}
	
	// Update is called once per frame
	void Update () {
        rig.AddRelativeForce(new Vector3(-posInicial.x * fuerza, posInicial.y, posInicial.z));
       

	}
}
