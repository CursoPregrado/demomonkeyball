﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Comportamientocubo2 : MonoBehaviour {

    // Use this for initialization
    public Rigidbody rig;
    private Vector3 posInicial;
    public int fuerza = 5;
    void Start()
    {
        rig = this.GetComponent<Rigidbody>();
        posInicial = this.GetComponent<Transform>().position;
    }

    // Update is called once per frame
    void Update()
    {
        if (rig.position.z > 24)
        {
            rig.AddRelativeForce(new Vector3(posInicial.x, posInicial.y, - posInicial.z * fuerza));
        }
        if (rig.position.z < 27.5)
        {
            rig.AddRelativeForce(new Vector3(posInicial.x, posInicial.y, posInicial.z * fuerza));
        }
    }
}
