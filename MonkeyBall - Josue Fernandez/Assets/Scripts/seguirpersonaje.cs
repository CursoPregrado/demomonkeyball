﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class seguirpersonaje : MonoBehaviour {

    private NavMeshAgent agente;
    public Transform destino;
	// Use this for initialization
	void Start () {
        agente = GetComponent<NavMeshAgent>();
    }
	
	// Update is called once per frame
	void Update () {
        agente.SetDestination(destino.position);
	}
}
