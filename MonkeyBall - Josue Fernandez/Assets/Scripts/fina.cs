﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class fina : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject.CompareTag("Player"))
        {
            SceneManager.UnloadSceneAsync(5);
            SceneManager.LoadScene("scene win");

        }
        if (collider.gameObject.CompareTag("Mariposa"))
        {
            SceneManager.LoadScene("scene lose");
        }
    }
}
