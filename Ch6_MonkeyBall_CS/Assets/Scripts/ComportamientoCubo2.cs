﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ComportamientoCubo : MonoBehaviour {

	// Use this for initialization
	public Rigidbody rig;
	private Vector3 posInicial;
	public int fuerza = 5;
	void Start () {
		rig = this.GetComponent<Rigidbody> ();
		posInicial = this.GetComponent<Transform> ().position;
	}

	// Update is called once per frame
	void Update()
	{
		if(rig.position.z > 25)
		{
			rig.AddRelativeForce(new Vector3(-posInicial.x * fuerza, posInicial.y, posInicial.z));
		}
		if(rig.position.x < 27)
		{
			rig.AddRelativeForce(new Vector3(posInicial.x * fuerza, posInicial.y, posInicial.z));
		}
	}
}