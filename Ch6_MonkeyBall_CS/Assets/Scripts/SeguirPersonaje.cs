﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class SeguirPersonaje : MonoBehaviour {

	private NavMeshAgent agente;
	public Transform Destino;

	// Use this for initialization
	void Start () {

		agente = GetComponent <NavMeshAgent> ();
	}

	// Update is called once per frame
	void Update () {

		agente.SetDestination(Destino.position);

	}
}