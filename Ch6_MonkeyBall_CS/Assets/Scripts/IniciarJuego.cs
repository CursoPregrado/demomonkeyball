﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEditor.SceneManagement;

public class IniciarJuego : MonoBehaviour {

	public void InicializarJuego(){

		SceneManager.LoadScene ("MonkeyBall");

	}

	public void FinalizarJuego(){

		Application.Quit ();

	}

	public void Continuar2(){

		SceneManager.LoadScene ("nivel2");

	}


	public void Continuar3(){

		SceneManager.LoadScene ("nivel3");

	}

}