﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Perseguir : MonoBehaviour {

	private UnityEngine.AI.NavMeshAgent agente;
	public Transform destino;

	// Use this for initialization
	void Start () {

		agente = GetComponent <UnityEngine.AI.NavMeshAgent> ();
		
	}
	
	// Update is called once per frame
	void Update () {

		agente.SetDestination (destino.position);
		
	}
}
