﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Comportamiento : MonoBehaviour {

	public Rigidbody rig;
	private Vector3 posInicial;

	// Use this for initialization
	void Start () {

		rig=this.GetComponent<Rigidbody> ();
		posInicial = this.GetComponent<Transform> ().position;
		
	}
	
	// Update is called once per frame
	void Update () {
		if (transform.position.x<=-1.5) {
			rig.AddForce (new Vector3(-transform.position.x*100, posInicial.y,posInicial.z));
		}
		if (transform.position.x>=1.9) {
			rig.AddForce (new Vector3(-transform.position.x*100, posInicial.y,posInicial.z));
		}

	}
}
